class Departamento
    attr_accessor :funcionarios, :nome, :gerente, :id
    @gerente
    @salarios = 0

    def initialize params
        @funcionarios = []
        @nome = params[:nome]
        @id = params[:id]
    end

    def add_func funcionario
        @funcionarios.append(funcionario)
        puts "#{funcionario.primeiro_nome} #{funcionario.segundo_nome} foi adicionado ao departamento"
    end

    def calc_gastos
        @funcionarios.each do |v|
            @salarios = @salarios + v.salario
        end
    end

    def num_func
        @funcionarios.length
    end

    def add_gerente gerente
        @gerente = gerente
        puts "#{gerente.primeiro_nome} #{gerente.segundo_nome} é o gerente do departamento"
    end

end