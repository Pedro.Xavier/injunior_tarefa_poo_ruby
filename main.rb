require './bd.rb'
require "faker"


funcionario = Funcionario.new({
    primeiro_nome: Faker::Name.first_name,
    segundo_nome: Faker::Name.middle_name,
    idade: Faker::Number.between(from: 18, to: 60),
    salario: 2500,
    endereco: Faker::Address.street_address,
    setor: Faker::Appliance.brand,
    matricula: Faker::Number.number(digits: 4),
    id: 1
})

dependente = Dependente.new({
    nome: Faker::Name.first_name,
    idade: Faker::Number.between(from: 18, to: 60),
    parentesco: "parente",
    id: 101
})

departamento = Departamento.new({
    nome: "IC_UFF",
    id: 55
})

gerente = Funcionario.new({
    primeiro_nome: Faker::Name.first_name,
    segundo_nome: Faker::Name.middle_name,
    idade: Faker::Number.between(from: 18, to: 60),
    salario: 4000,
    endereco: Faker::Address.street_address,
    setor: Faker::Appliance.brand,
    matricula: Faker::Number.number(digits: 4),
    id: 13
})

projeto = Projeto.new({
    nome: "Adote um Lobinho",
    cliente: "INJunior",
    fundos: 10000.00,
    id: 72
})

def cadastra_funcionario (funcionario)
    Bd.adiciona "funcionarios", funcionario
    puts "O #{funcionario.primeiro_nome} #{funcionario.segundo_nome} foi cadastrado com sucesso!" 
end

def cadastra_dependente (dependente)
    Bd.adiciona "dependentes", dependente
    puts "O #{dependente.nome} foi cadastrado com sucesso!" 
end

def cadastra_departamento (departamento)
    Bd.adiciona "departamentos", departamento
    puts "O #{departamento.nome} foi cadastrado com sucesso!" 
end

def cadastra_projeto (projeto)
    Bd.adiciona "projetos", projeto
    puts "O #{projeto.nome} foi cadastrado com sucesso!" 
end


cadastra_funcionario(funcionario)
cadastra_departamento(departamento)
cadastra_dependente(dependente)
cadastra_projeto(projeto)

puts funcionario.nome_completo
puts funcionario.add_dep(dependente)
puts funcionario.num_dep

puts departamento.add_func(funcionario)
# puts departamento.calc_gastos
puts departamento.num_func
puts departamento.add_gerente(gerente)

puts projeto.add_depto(departamento)


