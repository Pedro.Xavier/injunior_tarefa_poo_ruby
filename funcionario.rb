class Funcionario
    attr_accessor :primeiro_nome, :segundo_nome, :idade, :salario, :endereço,
    :dependentes, :setor, :matricula, :id
    
    def initialize params
        @primeiro_nome = params[:primeiro_nome]
        @segundo_nome = params[:segundo_nome]
        @idade = params[:idade]
        @salario = params[:salario]
        @endereco = params[:endereco]
        @dependentes = []
        @setor = params[:setor]
        @matricula = params[:matricula]
        @id = params[:id]
    end

    def nome_completo
        "#{@primeiro_nome} #{segundo_nome}"
    end

    def add_dep dependente
        @dependentes.append(dependente)
    end

    def num_dep
        @dependentes.length    
    end

end