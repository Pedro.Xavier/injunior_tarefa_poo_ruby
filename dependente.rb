class Dependente
    attr_accessor :nome, :idade, :parentesco, :id

    def initialize params
        @nome = params[:nome]
        @idade = params[:idade]
        @parentesco = params[:parentesco]
        @id = params[:id]
    end
end