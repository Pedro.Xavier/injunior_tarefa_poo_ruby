require './funcionario.rb'
require './dependente.rb'
require './projeto.rb'
require './departamento'

class Bd
    attr_accessor :funcionarios

    @@funcionarios = []
    @@projetos = []
    @@dependentes = []
    @@departamentos = []

    def self.adiciona vetor, objeto
        case vetor
        when "funcionarios"
            @@funcionarios.append(objeto)
        when "projetos"
            @@projetos.append(objeto)
        when "dependentes"
            @@dependentes.append(objeto)
        when "departamentos"
            @@departamentos.append(objeto)
        else
            puts "Erro"
        end
    end
    
    def self.busca_funcionario id
        @@funcionarios.each{|funcionario| return funcionario if funcionario.id == id}
    end

    def self.busca_dependentes id
        @@dependentes.each{|dependente| return funcionario if dependente.id == id}
    end

    def self.busca_departamentos id
        @@departamentos.each{|departamento| return departamento if departamento.id == id}
    end

    def self.busca_projetos id
        @@projetos.each{|projeto| return projeto if projeto.id == id}
    end

end